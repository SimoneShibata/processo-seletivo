package br.com.texoit.processoseletivo.dto;

import java.util.ArrayList;
import java.util.List;

public class AwardIntervalMaxAndMinDTO {

	private List<AwardIntervalDTO> min = new ArrayList<>();

	private List<AwardIntervalDTO> max = new ArrayList<>();

	public List<AwardIntervalDTO> getMin() {
		return min;
	}

	public void setMin(List<AwardIntervalDTO> min) {
		this.min = min;
	}

	public List<AwardIntervalDTO> getMax() {
		return max;
	}

	public void setMax(List<AwardIntervalDTO> max) {
		this.max = max;
	}

	public static AwardIntervalMaxAndMinDTO addMinAndMax(AwardIntervalDTO minInterval, AwardIntervalDTO maxInterval) {
		AwardIntervalMaxAndMinDTO dto = new AwardIntervalMaxAndMinDTO();
		
		if (minInterval != null) {			
			dto.getMin().add(minInterval);
		}
		if (maxInterval != null) {			
			dto.getMax().add(maxInterval);
		}
		
		return dto;
	}	
	
}
