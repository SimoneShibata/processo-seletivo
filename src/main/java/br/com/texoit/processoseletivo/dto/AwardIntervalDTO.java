package br.com.texoit.processoseletivo.dto;

public class AwardIntervalDTO {

	private String producer;
	private Integer previousWin;
	private Integer followingWin;

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public Integer getPreviousWin() {
		return previousWin;
	}

	public void setPreviousWin(Integer previousWin) {
		this.previousWin = previousWin;
	}

	public Integer getFollowingWin() {
		return followingWin;
	}

	public void setFollowingWin(Integer followingWin) {
		this.followingWin = followingWin;
	}

	public Integer getInterval() {
		return this.followingWin - this.previousWin;
	}

}
