package br.com.texoit.processoseletivo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.texoit.processoseletivo.domain.Movie;
import br.com.texoit.processoseletivo.dto.AwardIntervalDTO;
import br.com.texoit.processoseletivo.dto.AwardIntervalMaxAndMinDTO;
import br.com.texoit.processoseletivo.repository.MovieRepository;

@Service
public class ProducerService {

	@Autowired
	private MovieRepository movieRepository;

	public AwardIntervalMaxAndMinDTO getAwardsInterval() {
		List<AwardIntervalDTO> awardsInterval = new ArrayList<>();

		this.getMoviesWinnersSplitProducers().stream().collect(Collectors.groupingBy(Movie::getProducers))
				.forEach((producers, movies) -> {
					if (movies.size() > 1) {
						movies.stream().sorted(Comparator.comparing(Movie::getYear).reversed())
								.reduce((accumulator, current) -> {
									AwardIntervalDTO interval = new AwardIntervalDTO();
									interval.setProducer(producers);
									interval.setPreviousWin(current.getYear());
									interval.setFollowingWin(accumulator.getYear());

									awardsInterval.add(interval);

									return current;
								});

					}
				});

		AwardIntervalDTO minInterval = awardsInterval.stream().min(Comparator.comparing(AwardIntervalDTO::getInterval))
				.orElse(null);
		AwardIntervalDTO maxInterval = awardsInterval.stream().max(Comparator.comparing(AwardIntervalDTO::getInterval))
				.orElse(null);

		return AwardIntervalMaxAndMinDTO.addMinAndMax(minInterval, maxInterval);
	}

	private List<Movie> getMoviesWinnersSplitProducers() {

		List<Movie> moviesWinnersSplitProducers = new ArrayList<>();

		movieRepository.findByWinnerTrue().forEach(movie -> {
			Arrays.stream(movie.getProducers().split(", | and "))
					.forEach(producer -> moviesWinnersSplitProducers
							.add(new Movie.Builder()
											.id(movie.getId())
											.studios(movie.getStudios())
											.title(movie.getTitle())
											.year(movie.getYear())
											.producers(producer)
											.build())
											);
		});

		return moviesWinnersSplitProducers;
	}


}
