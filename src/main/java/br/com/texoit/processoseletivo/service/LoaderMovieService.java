package br.com.texoit.processoseletivo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.texoit.processoseletivo.domain.Movie;

@Service
public class LoaderMovieService {
	
	@Autowired
	private MovieService movieService;
	
	public void loadMovies(String row) {		
		String[] columns = row.split(";");
		
		Movie movie = new Movie();
		movie.setYear(Integer.parseInt(columns[0]));
		movie.setTitle(columns[1]);
		movie.setStudios(columns[2]);
		movie.setProducers(columns[3]);
		if (columns.length == 5) {			
			movie.setWinner(convertToBoolean(columns[4]));
		}
		
		this.movieService.save(movie);
	}

	private Boolean convertToBoolean(String winner) {
		if (winner.equals("yes")) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
}
