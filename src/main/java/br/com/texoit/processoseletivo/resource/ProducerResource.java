package br.com.texoit.processoseletivo.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.texoit.processoseletivo.dto.AwardIntervalMaxAndMinDTO;
import br.com.texoit.processoseletivo.service.ProducerService;

@RestController
@RequestMapping("/api/producers")
public class ProducerResource {

	@Autowired
	public ProducerService producerService;

	@GetMapping("/adwards-interval")
	public ResponseEntity<AwardIntervalMaxAndMinDTO> getAwardsInterval() {
		return ResponseEntity.ok(this.producerService.getAwardsInterval());
	}

}
