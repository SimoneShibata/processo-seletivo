package br.com.texoit.processoseletivo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Movie {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private int year;

	@NotNull
	private String title;

	@NotNull
	private String studios;

	@NotNull
	private String producers;

	private Boolean winner = Boolean.FALSE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public Boolean getWinner() {
		return winner;
	}

	public void setWinner(Boolean winner) {
		this.winner = winner;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public static class Builder {

		private Long id;
		private int year;
		private String title;
		private String studios;
		private String producers;
		private Boolean winner;

		public Builder() {
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder year(int year) {
			this.year = year;
			return this;
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder studios(String studios) {
			this.studios = studios;
			return this;
		}

		public Builder producers(String producers) {
			this.producers = producers;
			return this;
		}

		public Builder winner(Boolean winner) {
			this.winner = winner;
			return this;
		}

		public Movie build() {
			Movie movie = new Movie();
			movie.id = this.id;
			movie.year = this.year;
			movie.title = this.title;
			movie.studios = this.studios;
			movie.producers = this.producers;
			movie.winner = this.winner;

			return movie;
		}
	}

}
