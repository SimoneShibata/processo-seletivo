package br.com.texoit.processoseletivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.texoit.processoseletivo.domain.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	List<Movie> findByWinnerTrue();
	
}
