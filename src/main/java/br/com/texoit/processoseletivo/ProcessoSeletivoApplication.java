package br.com.texoit.processoseletivo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.texoit.processoseletivo.service.LoaderMovieService;

@SpringBootApplication
public class ProcessoSeletivoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ProcessoSeletivoApplication.class, args);
		
		try {
			Files.lines(Paths.get("src/main/resources/static/movielist.csv"))
			 .skip(1)
			 .forEach(ApplicationContextHolder.getContext().getBean(LoaderMovieService.class)::loadMovies);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
