package br.com.texoit.processoseletivo.resource;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.texoit.processoseletivo.ProcessoSeletivoApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProcessoSeletivoApplication.class)
@AutoConfigureMockMvc
@Transactional
@Rollback
public class ProducerResourceTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@Sql(statements = {
			"insert into movie(id, year, title, studios, producers, winner) values(1, 1990, 'Filme 1', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(2, 1991, 'Filme 2', 'Studio A', 'Produtor A', false)",
			"insert into movie(id, year, title, studios, producers, winner) values(3, 1991, 'Filme 3', 'Studio A', 'Produtor B', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(4, 2000, 'Filme 4', 'Studio A', 'Produtor A', true)"
	})
	public void getAwardsIntervalWhenOnlyOneProducerHasMoreThanOneAward() throws Exception {

		mockMvc.perform(get("/api/producers/adwards-interval"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("min[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("min[*].interval", hasItem(10)))
				.andExpect(jsonPath("min[*].previousWin", hasItem(1990)))
				.andExpect(jsonPath("min[*].followingWin", hasItem(2000)))
				.andExpect(jsonPath("max[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("max[*].interval", hasItem(10)))
				.andExpect(jsonPath("max[*].previousWin", hasItem(1990)))
				.andExpect(jsonPath("max[*].followingWin", hasItem(2000)));
	}

	@Test
	@Sql(statements = {
			"insert into movie(id, year, title, studios, producers, winner) values(1, 1990, 'Filme 1', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(2, 1991, 'Filme 2', 'Studio A', 'Produtor A', false)",
			"insert into movie(id, year, title, studios, producers, winner) values(3, 1991, 'Filme 3', 'Studio A', 'Produtor B', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(4, 2000, 'Filme 4', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(5, 2003, 'Filme 5', 'Studio A', 'Produtor B', true)"
	})
	public void getAwardsInterval() throws Exception {

		mockMvc.perform(get("/api/producers/adwards-interval"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("min[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("min[*].interval", hasItem(10)))
				.andExpect(jsonPath("min[*].previousWin", hasItem(1990)))
				.andExpect(jsonPath("min[*].followingWin", hasItem(2000)))
				.andExpect(jsonPath("max[*].producer", hasItem("Produtor B")))
				.andExpect(jsonPath("max[*].interval", hasItem(12)))
				.andExpect(jsonPath("max[*].previousWin", hasItem(1991)))
				.andExpect(jsonPath("max[*].followingWin", hasItem(2003)));
	}

	@Test
	@Sql(statements = {
			"insert into movie(id, year, title, studios, producers, winner) values(1, 1990, 'Filme 1', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(2, 1991, 'Filme 2', 'Studio A', 'Produtor A', false)",
			"insert into movie(id, year, title, studios, producers, winner) values(3, 1991, 'Filme 3', 'Studio A', 'Produtor B', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(4, 2000, 'Filme 4', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(5, 2001, 'Filme 5', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(6, 2003, 'Filme 6', 'Studio A', 'Produtor B', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(7, 2018, 'Filme 7', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(8, 2019, 'Filme 8', 'Studio A', 'Produtor A', true)"
	})
	public void getAwardsIntervalWhenProducerHasMoreThan2Movies() throws Exception {

		mockMvc.perform(get("/api/producers/adwards-interval"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("min[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("min[*].interval", hasItem(1)))
				.andExpect(jsonPath("min[*].previousWin", hasItem(2018)))
				.andExpect(jsonPath("min[*].followingWin", hasItem(2019)))
				.andExpect(jsonPath("max[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("max[*].interval", hasItem(17)))
				.andExpect(jsonPath("max[*].previousWin", hasItem(2001)))
				.andExpect(jsonPath("max[*].followingWin", hasItem(2018)));
	}

	@Test
	@Sql(statements = {
			"insert into movie(id, year, title, studios, producers, winner) values(1, 1990, 'Filme 1', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(2, 1991, 'Filme 2', 'Studio A', 'Produtor A', false)",
			"insert into movie(id, year, title, studios, producers, winner) values(3, 1991, 'Filme 3', 'Studio A', 'Produtor B', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(4, 2000, 'Filme 4', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(5, 2001, 'Filme 5', 'Studio A', 'Produtor B', true)"
	})
	public void getAwardsIntervalWhenIntervalEquals() throws Exception {

		mockMvc.perform(get("/api/producers/adwards-interval"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("min[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("min[*].interval", hasItem(10)))
				.andExpect(jsonPath("min[*].previousWin", hasItem(1990)))
				.andExpect(jsonPath("min[*].followingWin", hasItem(2000)))
				.andExpect(jsonPath("max[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("max[*].interval", hasItem(10)))
				.andExpect(jsonPath("max[*].previousWin", hasItem(1990)))
				.andExpect(jsonPath("max[*].followingWin", hasItem(2000)));
	}

	@Test
	@Sql(statements = {
			"insert into movie(id, year, title, studios, producers, winner) values(1, 1990, 'Filme 1', 'Studio A', 'Produtor A', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(2, 1995, 'Filme 2', 'Studio A', 'Produtor A, Produtor B and Produtor C', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(3, 2001, 'Filme 3', 'Studio A', 'Produtor B and Produtor C', true)",
			"insert into movie(id, year, title, studios, producers, winner) values(4, 2003, 'Filme 4', 'Studio A', 'Produtor B and Produtor C', false)"
	})
	public void getAwardsIntervalWhenMovieHasMoreThanOneProducer() throws Exception {

		mockMvc.perform(get("/api/producers/adwards-interval"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("min[*].producer", hasItem("Produtor A")))
				.andExpect(jsonPath("min[*].interval", hasItem(5)))
				.andExpect(jsonPath("min[*].previousWin", hasItem(1990)))
				.andExpect(jsonPath("min[*].followingWin", hasItem(1995)))
				.andExpect(jsonPath("max[*].producer", hasItem("Produtor C")))
				.andExpect(jsonPath("max[*].interval", hasItem(6)))
				.andExpect(jsonPath("max[*].previousWin", hasItem(1995)))
				.andExpect(jsonPath("max[*].followingWin", hasItem(2001)));
	}
}
