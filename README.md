Levantar aplicação:

`gradlew bootRun`

Endpoint do intervalo de prêmios

`(GET) http://localhost:8880/api/producers/adwards-interval`

Para acessar o banco em memória H2

`http://localhost:8880/h2`

user: `processoseletivo`

password: `texoit`

Rodar testes:

`gradlew test`